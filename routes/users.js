const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const model = require('../models/index');
const QueryTypes  = require('sequelize');
const passportJWT = require('../middlewares/passport-jwt');
const jwt = require('jsonwebtoken');

router.get('/profile',[passportJWT.isLogin], async function(req, res, next){
  
  const user = await model.User.findByPk(req.user.user_id);
  
  return res.status(200).json({
    user : {
      id : user.id,
      fullname : user.fullname,
      email : user.email,
      created_at : user.created_at
    }
  });
});

/* GET users listing. */
router.get('/', async function(req, res, next) {

  const users = await model.User.findAll({
    //attributes : ['id' , 'fullname'] // get only [id , fullname]
    attributes : { exclude :['password']}, // get all wiht out password
    order : [['id','desc']] 
  });
  /*const sql = 'SELECT id,fullname,email FROM `users`';
  const users = await model.sequelize.query(sql, { 
    type: QueryTypes.SELECT 
  });*/

  const totalUser = await model.User.count(); // count all recode

  return res.status(200).json({
    total : totalUser,
    data : users
  });
});

router.post('/register', async function(req,res,next){

  const {fullname, email , password} = req.body;

  const userEmail = await model.User.findOne({ where : {email : email}});
  if(userEmail != null){
    return res.status(400).json({
      message : 'This email is registered'
    });
  }

  const passwordHash = await argon2.hash(password);
  
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(200).json({
    user : {
      id: newUser.id,
      fullname : newUser.fullname
    },
    message : 'register is success'
  });
});

router.post('/login', async function(req,res,next){

  const {email , password} = req.body;

  const user = await model.User.findOne({ where : {email : email}});
  if(user === null){
    return res.status(404).json({
      message : 'This email is not found'
    });
  }

  const isValid = await argon2.verify(user.password,password);

  if(!isValid){
    return res.status(401).json({
      message : 'Password is not correct'
    });
  } 

  const token = jwt.sign({ user_id : user.id}, process.env.JWT_KEY, { expiresIn : '7d'});

  return res.status(200).json({
    message : 'Login success',
    access_token : token
  });
});



module.exports = router;